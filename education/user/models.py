from django.db import models


class Student(models.Model):
    """
    Модель "Студент"
    """
    firstname = models.CharField("Имя", max_length=20)
    lastname = models.CharField("Фамилия", max_length=20)
    email = models.EmailField("E-mail", blank=False, unique=True)
    password = models.CharField("Пароль", max_length=30, blank=False)

    class Meta:
        verbose_name = "Студент"
        verbose_name_plural = "Студенты"

    @property
    def full_name(self):
        return f"{self.firstname} {self.lastname}"

    def __str__(self):
        return self.full_name


class Teacher(Student, models.Model):
    """
    Модель "Преподаватель"
    """
    works = models.BooleanField("Действующий сотрудник", default=True)

    class Meta:
        verbose_name = "Преподаватель"
        verbose_name_plural = "Преподаватели"
