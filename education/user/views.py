from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from user.models import Teacher


class PageTitleMixin:
    """
    Добавляет возможность задать заголовок страницы

    page_title: заголовок страницы
    """
    page_title = ''

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = self.page_title
        return context


class TeacherListView(PageTitleMixin, ListView):
    model = Teacher
    page_title = "Список преподавателей"


class TeacherDetailView(DetailView):
    model = Teacher


class TeacherCreateView(PageTitleMixin, CreateView):
    model = Teacher
    success_url = reverse_lazy('user:teacher_list')
    fields = '__all__'
    page_title = 'Добавление преподавателя'


class TeacherUpdateView(PageTitleMixin, UpdateView):
    model = Teacher
    fields = '__all__'
    page_title = 'Редактирование данных о сотруднике'

    def get_success_url(self):
        return reverse_lazy('user:teacher_detail', kwargs={'pk': self.object.pk})


class TeacherDeleteView(DeleteView):
    model = Teacher
    success_url = reverse_lazy('user:teacher_list')
