from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView

from user.models import Teacher
from .models import Course, Category

app_name = "course"


class PageTitleMixin:
    """
    Добавляет возможность задать заголовок страницы

    page_title: заголовок страницы
    """
    page_title = ''

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = self.page_title
        return context


class CourseListView(PageTitleMixin, ListView):
    model = Course
    page_title = 'Список курсов'
    queryset = model.objects.filter(Q(status=Course.AVAILABLE) | Q(status=Course.PREPARING))


class CourseDetailView(DetailView):
    model = Course


class CourseCreateView(PageTitleMixin, CreateView):
    model = Course
    success_url = reverse_lazy('course:course_list')
    fields = '__all__'
    page_title = 'Создание курса'


class CourseUpdateView(PageTitleMixin, UpdateView):
    model = Course
    fields = '__all__'
    page_title = 'Редактирование курса'

    def get_success_url(self):
        return reverse_lazy('course:course_detail', kwargs={'pk': self.object.pk})


class CourseDeleteView(DeleteView):
    model = Course
    success_url = reverse_lazy('course:course_list')


class CategoryListView(PageTitleMixin, ListView):
    model = Category
    page_title = "Список категорий"


class CategoryDetailView(DetailView):
    model = Category


class CategoryCoursesDetailView(DetailView):
    model = Category

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['courses'] = Course.objects.filter(category=self.kwargs['pk'])
        return context


class CategoryCreateView(PageTitleMixin, CreateView):
    model = Category
    success_url = reverse_lazy('course:category_list')
    fields = '__all__'
    page_title = 'Добавление категории'


class CategoryUpdateView(PageTitleMixin, UpdateView):
    model = Category
    fields = '__all__'
    page_title = 'Редактирование категории'

    def get_success_url(self):
        return reverse_lazy('course:category_detail', kwargs={'pk': self.object.pk})


class CategoryDeleteView(DeleteView):
    model = Category
    success_url = reverse_lazy('course:category_list')


def index(request):
    teachers = Teacher.objects.all()[:10]
    categories = Category.objects.all()[:10]
    courses = Course.objects.all()[:10]

    context = {
        'page_title': 'Главная страница',
        'teachers': teachers,
        'categories': categories,
        'courses': courses,
    }
    return render(request, 'course/index.html', context)
