from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('course.urls', namespace='course')),
    path('', include('user.urls', namespace='user')),
    path('', include('about.urls', namespace='about')),
    path('api-auth/', include('rest_framework.urls')),
    path('api/v1/', include('api.urls', namespace='api')),
    path('admin/', admin.site.urls),
]
