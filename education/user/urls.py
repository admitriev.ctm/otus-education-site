from django.urls import path
import user.views as user_views

app_name = 'user'

urlpatterns = [
    path('teacher/', user_views.TeacherListView.as_view(), name="teacher_list"),
    path('teacher/<int:pk>/', user_views.TeacherDetailView.as_view(), name="teacher_detail"),
    path('teacher/<int:pk>/update/', user_views.TeacherUpdateView.as_view(), name="teacher_update"),
    path('teacher/<int:pk>/delete/', user_views.TeacherDeleteView.as_view(), name="teacher_delete"),
    path('teacher/create/', user_views.TeacherCreateView.as_view(), name="teacher_create"),
]
