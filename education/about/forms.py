from django import forms


class AboutForm(forms.Form):
    from_fullname = forms.CharField(label='Ваше ФИО')
    from_email = forms.EmailField(label='Ваш e-mail')
    subject = forms.CharField(label='Тема', max_length=200)
    message = forms.CharField(label='Сообщение', widget=forms.Textarea)
