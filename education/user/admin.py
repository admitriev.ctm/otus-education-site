from django.contrib import admin
from .models import Student, Teacher


@admin.register(Student)
class Student(admin.ModelAdmin):
    list_display = "id", "email", "full_name", "lastname"
    list_display_links = "email",


@admin.register(Teacher)
class Teacher(admin.ModelAdmin):
    list_display = "id", "email", "full_name", "works"
    list_display_links = "email",
    list_filter = "works",
