from django.urls import path
import about.views as about_views

app_name = 'about'

urlpatterns = [
    path('about/', about_views.about, name="about"),
    path('about/thanks/', about_views.thanks, name="thanks"),
]
