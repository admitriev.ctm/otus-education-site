from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.core.mail import send_mail
from .forms import AboutForm
from redis import Redis
from rq import Queue

INFO_EMAIL = 'info@mysite.ru'


def send_confirmation(to_email):
    subject = "Подтверждение получения"
    message = """Добрый день!
    
        Ваше сообщение успешно доставлено.
        Мы ответим вам в ближайшее время!
        
        С уважением,
        ..."""
    from_email = INFO_EMAIL
    recipients = [to_email]
    send_mail(subject, message, from_email, recipients)


def about(request):
    if request.method == 'POST':
        form = AboutForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            message = "СООБЩЕНИЕ ОТ:\n\n" + \
                      form.cleaned_data['from_fullname'] + \
                      "\n\nТЕКСТ СООБЩЕНИЯ:\n\n" + \
                      form.cleaned_data['message']
            from_email = form.cleaned_data['from_email']

            recipients = [INFO_EMAIL]

            q = Queue(connection=Redis())
            q.enqueue(send_mail, subject, message, from_email, recipients)
            q.enqueue(send_confirmation, from_email)
            return HttpResponseRedirect('thanks')
    else:
        form = AboutForm()

    return render(request, 'about/about_form.html', {'form': form})


def thanks(request):
    return render(request, 'about/thanks.html')
