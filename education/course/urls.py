from django.urls import path
import course.views as course_views

app_name = 'course'

urlpatterns = [
    path('', course_views.index, name="index"),
    path('course/', course_views.CourseListView.as_view(), name="course_list"),
    path('course/<int:pk>/', course_views.CourseDetailView.as_view(), name="course_detail"),
    path('course/<int:pk>/update', course_views.CourseUpdateView.as_view(), name="course_update"),
    path('course/<int:pk>/delete', course_views.CourseDeleteView.as_view(), name="course_delete"),
    path('course/create/', course_views.CourseCreateView.as_view(), name="course_create"),
    path('category/', course_views.CategoryListView.as_view(), name="category_list"),
    # path('category/<int:pk>/', course_views.CategoryDetailView.as_view(), name="category_detail"),
    path('category/<int:pk>/', course_views.CategoryCoursesDetailView.as_view(), name="category_detail"),
    path('category/<int:pk>/update', course_views.CategoryUpdateView.as_view(), name="category_update"),
    path('category/<int:pk>/delete', course_views.CategoryDeleteView.as_view(), name="category_delete"),
    path('category/create/', course_views.CategoryCreateView.as_view(), name="category_create"),
]
